/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */

#include "fileserv.h"
#include <string.h>

struct lang_pref {
	char const *lang;
	size_t len;
	float pref;
};

static int
lang_pref_cmp(const void *a, const void *b)
{
	struct lang_pref const *lpa = a;
	struct lang_pref const *lpb = b;

	if (lpa->pref < lpb->pref)
		return 1;
	if (lpa->pref > lpb->pref)
		return -1;
	return 0;
}

static int
parse_accepted_language(const char *str, struct lang_pref **lpv, size_t *lpc)
{
	size_t count = 1;
	const char *p;
	struct lang_pref *lp;
	size_t len;
	size_t i;
	
	for (p = str; *p; p++)
		if (*p == ',')
			++count;
	lp = calloc(count, sizeof(lp[0]));
	if (!lp) {
		alloc_warn();
		return -1;
	}
	i = 0;
	while (i < count) {
		while (*str && (*str == ' ' || *str == '\t'))
			++str;
		if (!*str)
			break;
		len = strcspn(str, ",;");
		lp[i].lang = str;
		lp[i].len = len;
		str += len;
		if (*str == ';') {
			while (*++str && (*str == ' ' || *str == '\t'))
				;
			if (str[0] == 'q' && str[1] == '=') {
				lp[i].pref = strtod(str + 2, (char**) &str);
				while (*str && (*str == ' ' || *str == '\t'))
					++str;
			}
		} else {
			lp[i].pref = 1.0;
		}
		if (*str == ',') {
			++str;
			++i;
		} else {
			if (*str == 0)
				i++;
			break;
		}
	}
	qsort(lp, i, sizeof(lp[0]), lang_pref_cmp);
	
	*lpv = lp;
	*lpc = i;

	return 0;
}

static ssize_t
match_lang(struct lang_pref const *lpv, size_t lpc, char const *lang)
{
	size_t llen = strlen(lang);
	ssize_t i, wildcard = -1;
	
	for (i = 0; i < lpc; i++) {
		if (lpv[i].len == 1 && lpv[i].lang[0] == '*')
			wildcard = i;
		else if ((lpv[i].len == llen
			  || (lpv[i].len > llen && lpv[i].lang[llen] == '-'))
			 && memcmp(lpv[i].lang, lang, llen) == 0)
			return i;
	}
	return wildcard;
}

LANG_VAR *
select_language(LANG_VAR_LIST *lst, char const *acc)
{
	struct lang_pref *lpv;
	size_t lpc;
	LANG_VAR *res = NULL;
	ssize_t n;
	LANG_VAR *lv;
	
	if (parse_accepted_language(acc ? acc : "en,*;q=0.5", &lpv, &lpc)
	    == 0) {
		STAILQ_FOREACH(lv, lst, next) {
			ssize_t j = match_lang(lpv, lpc, lv->lang);
			if (j >= 0
			    && (!res || lpv[j].pref > lpv[n].pref)) {
				res = lv;
				n = j;
			}
		}
		free(lpv);
	}

	if (res == NULL) {
		res = STAILQ_FIRST(lst);
		STAILQ_FOREACH(lv, lst, next) {
			if (strcmp(lv->lang, "en") == 0) {
				res = lv;
				break;
			}
		}
	}
	return res;
}


/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */
#include "fileserv.h"
#include "dirls.h"
#include <errno.h>
#include <dirent.h>
#include <string.h>
#include "mimetypes.h"

void
dirls_init(DIRLS *lst)
{
	lst->count = 0;
	STAILQ_INIT(&lst->list);
}

void
dirls_free(DIRLS *lst)
{
	DIRLSENT *ent;
	
	while ((ent = STAILQ_FIRST(&lst->list))) {
		STAILQ_REMOVE_HEAD(&lst->list, next);
		free(ent);
	}
	lst->count = 0;
}

static void
dirls_append(DIRLS *lst, DIRLSENT *ent)
{
	STAILQ_INSERT_TAIL(&lst->list, ent, next);
	lst->count++;
}

int
dirls_scan(DIRLS *dirls, char const *path, CONFIG const *conf)
{
	DIR *dir;
	struct dirent *ent;
	DIRLSENT *dirlsent;
	int err = 0;
	
	dir = opendir(path);
	if (!dir) {
		err = errno;
		error("can't open directory %s: %s",
		      path, strerror(err));
		return err;
	}
	dirls_init(dirls);
	while (err == 0 && (ent = readdir(dir))) {
		size_t len;
		char *name;
		struct stat st;
		
		if (filename_is_hidden(ent->d_name, conf))
			continue;

		name = catfile(path, ent->d_name);
		if (!name) {
			err = errno;
			break;
		}
		
		if (stat(name, &st)) {
			error("can't stat %s: %s", name, strerror(errno));
			free(name);
			continue;
		}

		if (conf->list_unreadable == 0) {
			if (access(name, R_OK)) {
				free(name);
				continue;
			}
		}

		len = strlen(ent->d_name);
		dirlsent = malloc(sizeof(*dirlsent)
				+ len
				+ (S_ISDIR(st.st_mode) ? 1 : 0)
				+ 1);
		if (!dirlsent)
			err = errno;
		else {
			dirlsent->name = (char*)(dirlsent + 1);
			memcpy(dirlsent->name, ent->d_name, len);
			if (S_ISDIR(st.st_mode))
				dirlsent->name[len++] = '/';
			dirlsent->name[len] = 0;
			
			dirlsent->st = st;
			dirlsent->type = get_file_type(name);
			dirls_append(dirls, dirlsent);
		}
		free(name);
	}
	closedir(dir);

	if (err)
		dirls_free(dirls);
	
	return err;
}

static void
dirls_qsort(DIRLS *list, int cmp (DIRLSENT const *, DIRLSENT const *, void *),
	      void *data)
{
	if (list->count < 2)
		return;
	else if (list->count == 2) {
		DIRLSENT *a, *b;
		a = STAILQ_FIRST(&list->list);
		b = STAILQ_NEXT(a, next);
		if (cmp(a, b, data) > 0) {
			STAILQ_REMOVE_HEAD(&list->list, next);
			STAILQ_INSERT_TAIL(&list->list, a, next);
		}
	} else {
		DIRLSENT *cur, *middle;
		DIRLS high, low;
		int rc;
		
		cur = middle = STAILQ_FIRST(&list->list);
		do {
			cur = STAILQ_NEXT(cur, next);
			if (!cur)
				return;
		} while ((rc = cmp(middle, cur, data)) == 0);

		if (rc > 0)
			middle = cur;
		
		dirls_init(&high);
		dirls_init(&low);
		
		while ((cur = STAILQ_FIRST(&list->list))) {
			STAILQ_REMOVE_HEAD(&list->list, next);
			if (cmp(middle, cur, data) < 0)
				dirls_append(&high, cur);
			else
				dirls_append(&low, cur);
		}

		/* Sort both lists */
		dirls_qsort(&low, cmp, data);
		dirls_qsort(&high, cmp, data);

		/* Join lists in order and return */
		STAILQ_CONCAT(&low.list, &high.list);
		list->list = low.list;
		list->count = low.count + high.count;
	}
}

static char ordargs[] = "AD";
static char colargs[] = "NMSD";

INDEX_SORT_COL
index_sort_col_from_arg(int c)
{
	char const *p;
	if ((p = strchr(colargs, c)))
		return p - colargs;
	return ISC_NAME;
}

int
index_sort_col_to_arg(INDEX_SORT_COL c)
{
	return colargs[c];
}

INDEX_SORT_ORD
index_sort_ord_from_arg(int c)
{
	char const *p;
	if ((p = strchr(ordargs, c)))
		return p - ordargs;
	return ISO_ASC;
}

int
index_sort_ord_to_arg(INDEX_SORT_ORD c)
{
	return ordargs[c];
}

static inline int
comp_result(int val, void *data)
{
	if (*(INDEX_SORT_ORD*)data == ISO_DESC)
		val = -val;
	return val;
}

static int
comp_name(DIRLSENT const *a, DIRLSENT const *b, void *data)
{
	return comp_result(strcmp(a->name, b->name), data);
}

static int
comp_time(DIRLSENT const *a, DIRLSENT const *b, void *data)
{
	int r;
	if (a->st.st_mtime < b->st.st_mtime)
		r = -1;
	else if (a->st.st_mtime > b->st.st_mtime)
		r = 1;
	else
		r = 0;
	return comp_result(r, data);
}

static int
comp_size(DIRLSENT const *a, DIRLSENT const *b, void *data)
{
	int r;
	if (a->st.st_size < b->st.st_size)
		r = -1;
	else if (a->st.st_size > b->st.st_size)
		r = 1;
	else
		r = 0;
	return comp_result(r, data);
}

static int (*comp[])(DIRLSENT const *, DIRLSENT const *, void *) = {
	comp_name,
	comp_time,
	comp_size,
	comp_name
};

void
dirls_sort(DIRLS *dirls, INDEX_SORT_COL col, INDEX_SORT_ORD ord)
{	
	dirls_qsort(dirls, comp[col], &ord);
}


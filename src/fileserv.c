/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */

#include "fileserv.h"
#include "mimetypes.h"
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <netdb.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>
#include <ctype.h>

#ifndef DEFAULT_ADDRESS
# define DEFAULT_ADDRESS "0.0.0.0"
#endif
#ifndef DEFAULT_SERVICE
# define DEFAULT_SERVICE "8080"
#endif

char const *progname;
int verbose; /* increase diagnostic verbosity */
char *address = DEFAULT_ADDRESS;
char *forwarded_header = "X-Forwarded-For";
char *index_css;
char *tmpdir;
char *user;
char *group;
char *mime_types_file;

void
usage(void)
{
	printf("usage: %s [OPTIONS] [HOST:]URL:DIR...\n", progname);
	printf("Simple HTTP server.\n");
	printf("OPTIONS are:\n\n");
        printf("  -a [IP]:PORT         listen on this IP and PORT\n");
	printf("  -c FILE              read configuration from FILE\n");
	printf("  -f                   remain in the foreground\n");
        printf("  -g GROUP             run with this group privileges\n");
	printf("  -h                   show this help summary\n");
        printf("  -s                   don't start sentinel process\n");
        printf("  -u USER              run with this user privileges\n");
	printf("  -v                   increase verbosity\n");
	printf("  -V                   show program version\n");
	printf("\n");
}

const char version_copyright[] = "Copyright (C) 2017-2023 Sergey Poznyakoff";
const char gplv3[] = "\
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\
";

static void
show_version(void)
{
	printf("%s\n", PACKAGE_STRING);
	printf("%s\n", version_copyright);
	printf("\n%s\n\n", gplv3);
}

static void
fileserv_panic(void *cls, const char *file, unsigned int line,
	     const char *reason)
{
        if (reason)
		error("%s:%d: MHD PANIC: %s", file, line, reason);
	else
		error("%s:%d: MHD PANIC", file, line);
	abort();
}

static inline int
fse_to_http_code(int ec)
{
	switch (ec) {
	case FSE_SUCCESS:
		return MHD_HTTP_OK;
	case FSE_SYNTAX:
	case FSE_ACCESS:
		return MHD_HTTP_FORBIDDEN;
	case FSE_ERROR:
		break;
	}
	return MHD_HTTP_INTERNAL_SERVER_ERROR;
}
		
static inline int
errno_to_http_code(int ec)
{
	switch (ec) {
	case EINVAL:
	case ENOSYS:
	case ENOMEM:
		return MHD_HTTP_INTERNAL_SERVER_ERROR;
	case ENOENT:
		return MHD_HTTP_NOT_FOUND;
	default:
		return MHD_HTTP_FORBIDDEN;
	}
}

int
open_node(char const *node, char const *serv, struct sockaddr **saddr)
{
	struct addrinfo hints;
	struct addrinfo *res;
	int reuse;
	int fd;
	int rc;
	
	memset(&hints, 0, sizeof (hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	rc = getaddrinfo(node, serv, &hints, &res);
	if (rc) {
		error("%s: %s", node, gai_strerror(rc));
		exit(1);
	}

	fd = socket(res->ai_family, res->ai_socktype, 0);
	if (fd == -1) {
		error("socket: %s", strerror(errno));
		exit(1);
	}

	reuse = 1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR,
		   &reuse, sizeof(reuse));
	if (bind(fd, res->ai_addr, res->ai_addrlen) == -1) {
		error("bind: %s", strerror(errno));
		exit(1);
	}

	if (listen(fd, 8) == -1) {
		error("listen: %s", strerror(errno));
		exit(1);
	}

        if (saddr) {
		*saddr = xmalloc(res->ai_addrlen);
		memcpy(*saddr, res->ai_addr, res->ai_addrlen);
	}
	freeaddrinfo(res);

	return fd;
}

int
open_listener(char const *addr, struct sockaddr **saddr)
{
	size_t len;
	int fd;
	
	len = strcspn(addr, ":");
	if (len == 0)
		fd = open_node(DEFAULT_ADDRESS, addr + 1, saddr);
	else if (addr[len] == 0)
		fd = open_node(addr, DEFAULT_SERVICE, saddr);
	else {
		char *node;
		node = xmalloc(len + 1);
		memcpy(node, addr, len);
		node[len] = 0;
		fd = open_node(node, addr + len + 1, saddr);
		free(node);
	}
	return fd;
}

struct urimap {
	char *host;
	char *uri;
	size_t uri_len;
	char *dir;
	size_t dir_len;
	TAILQ_ENTRY(urimap) next;
};

TAILQ_HEAD(,urimap) map_head = TAILQ_HEAD_INITIALIZER(map_head);

struct urimap const *
urimap_find(char const *host, char const *url)
{
	struct urimap *map;
	size_t len = strlen(url);

	TAILQ_FOREACH(map, &map_head, next) {
		if (map->host && (!host || strcasecmp(map->host, host)))
			continue;
		if (map->uri_len > len)
			continue;
		if (memcmp(url, map->uri, map->uri_len) == 0
		    && (url[map->uri_len] == '/' || url[map->uri_len] == 0))
			return map;
	}
	return NULL;
}	

struct urimap const *
urimap_find_dir(char const *host, char const *filename)
{
	struct urimap *map;
	size_t len = strlen(filename);

	TAILQ_FOREACH(map, &map_head, next) {
		if (map->host && strcasecmp(map->host, host))
			continue;
		if (map->dir_len > len)
			continue;
		if (memcmp(filename, map->dir, map->dir_len) == 0
		    && (filename[map->dir_len] == 0
			|| filename[map->dir_len] == '/'))
			return map;
	}
	return NULL;
}	

void
urimap_add(char *arg)
{
	struct urimap *map, *cur;
	char *copy, *p;
	
	map = xmalloc(sizeof(*map) + strlen(arg) + 1);
	copy = (char*)(map + 1);
	strcpy(copy, arg);
	p = strchr(copy, ':');
	if (p) {
		*p++ = 0;
		map->dir = strchr(p, ':');
		if (map->dir) {
			map->host = copy;
			map->uri = p;
			*map->dir++ = 0;
		} else {
			map->host = NULL;
			map->uri = copy;
			map->dir = p;
		}
	} else {
		map->host = NULL;
		map->uri = copy;
		map->dir = NULL;
	}
	map->uri_len = strlen(map->uri);
	while (map->uri_len > 0 && map->uri[map->uri_len-1] == '/')
		map->uri[--map->uri_len] = 0;
	if (map->dir) {
		map->dir_len = strlen(map->dir);
		while (map->dir_len > 0 && map->dir[map->dir_len-1] == '/')
			map->dir[--map->dir_len] = 0;
	}

	/* Make sure the list is sorted by uri_len in descending order */
	TAILQ_FOREACH(cur, &map_head, next) {
		if (cur->uri_len < map->uri_len) {
			TAILQ_INSERT_BEFORE(cur, map, next);
			return;
		}
	}
	TAILQ_INSERT_TAIL(&map_head, map, next);
}

static char *
cfname(char const *fname)
{
	/* FIXME */
	return realpath(fname, NULL);
}

static int
find_index_file(char const *dir, CONFIG *conf, char **index_file,
		struct stat *pst)
{
	char *cf;
	
	if (conf->index_files_v) {
		int i;
		int lasterr = 0;
		for (i = 0; conf->index_files_v[i]; i++) {
			struct stat st;
			cf = catfile(dir, conf->index_files_v[i]);
			if (!cf) {
				lasterr = errno;
				break;
			}
			if (access(cf, F_OK) || lstat(cf, &st)) {
				free(cf);
				lasterr = errno;
				continue;
			}
			if (S_ISDIR(st.st_mode)) {
				free(cf);
				lasterr = ENOENT;
				continue;
			}
			*index_file = cf;
			*pst = st;
			return MHD_HTTP_OK;
		}
		return errno_to_http_code(lasterr);
	}
	return MHD_HTTP_FORBIDDEN;
}

typedef struct file_resp {
	int fd;
	char *file_name;
	CONFIG *conf;
	struct stat st;
	char const *type;
	char *location;
} FILE_RESP;

static void
file_resp_init(FILE_RESP *resp)
{
	memset(resp, 0, sizeof(*resp));
	resp->fd = -1;
}

static void
file_resp_free(FILE_RESP *resp)
{
	if (resp->fd >= 0)
		close(resp->fd);
	free(resp->file_name);
	config_free(resp->conf);
	free(resp->location);
}

static char *
make_location(char const *proto, char const *host, char const *url)
{
	size_t len;
	char *buf;

	len = strlen(proto) + 3 + strlen(host) + strlen(url) + 1;
	if (url[0] != '/')
		len++;
	buf = malloc(len + 1);
	if (buf) {
		strcpy(buf, proto);
		strcat(buf, "://");
		strcat(buf, host);
		if (url[0] != '/')
			strcat(buf, "/");
		strcat(buf, url);
		strcat(buf, "/");
	}
	return buf;
}

static INDEX_SORT_COL
get_sort_col(struct MHD_Connection *conn)
{
	char const *s = MHD_lookup_connection_value(conn,
						    MHD_GET_ARGUMENT_KIND,
						    "C");
	return s ? index_sort_col_from_arg(s[0]) : ISC_NAME;
}

static INDEX_SORT_COL
get_sort_ord(struct MHD_Connection *conn)
{
	char const *s = MHD_lookup_connection_value(conn,
						    MHD_GET_ARGUMENT_KIND,
						    "O");
	return s ? index_sort_ord_from_arg(s[0]) : ISO_ASC;
}

static char const *
basename(char const *s)
{
	char *p = strrchr(s, '/');
	return p ? p + 1 : s;
}

int
get_file_resp(struct MHD_Connection *conn, char const *url, FILE_RESP *resp)
{
	struct urimap const *map;
	char *cf;
	char const *host = MHD_lookup_connection_value(conn,
						       MHD_HEADER_KIND,
						       MHD_HTTP_HEADER_HOST);
	int rc;
	
	file_resp_init(resp);

	if (!filename_is_valid(url))
		return MHD_HTTP_NOT_FOUND;
	
	map = urimap_find(host, url);
	if (!map)
		return MHD_HTTP_NOT_FOUND;
	resp->file_name = catfile_n(map->dir, map->dir_len,
				    url + map->uri_len);
	if (!resp->file_name)
		return MHD_HTTP_INTERNAL_SERVER_ERROR;
	if (lstat(resp->file_name, &resp->st))
		return errno_to_http_code(errno);
	
	rc = dirconfig(resp->file_name, map->dir_len, &resp->conf);
	if (rc)
		return fse_to_http_code(rc);
	
	if (filename_is_hidden(basename(resp->file_name), resp->conf))
		return MHD_HTTP_NOT_FOUND;		

	if (S_ISLNK(resp->st.st_mode)) {
		if (!resp->conf->follow)
			return MHD_HTTP_FORBIDDEN;

		cf = cfname(resp->file_name);
		if (!cf) {
			return MHD_HTTP_NOT_FOUND;
		} else if (urimap_find_dir(host, cf)) {
			free(resp->file_name);
			resp->file_name = cf;
			stat(cf, &resp->st);
		} else {
			free(cf);
			return MHD_HTTP_NOT_FOUND;
		}
	}

	if (S_ISDIR(resp->st.st_mode)) {
		int res;
		
		if (url[strlen(url) - 1] != '/') {
			resp->location = make_location("http",
						       host, url);
			if (!resp->location)
				return MHD_HTTP_INTERNAL_SERVER_ERROR;
			
			return MHD_HTTP_MOVED_PERMANENTLY;
		}

		res = find_index_file(resp->file_name, resp->conf, &cf,
				      &resp->st);
		if (res == MHD_HTTP_OK) {
			free(resp->file_name);
			resp->file_name = cf;
		} else if (resp->conf->listing) {
			char *template;
			int fd;
			
			template = catfile(tmpdir, "idxXXXXXX");
			if (!template)
				return MHD_HTTP_INTERNAL_SERVER_ERROR;
				
			fd = mkstemp(template);
			if (fd == -1) {
				error("can't create temporary file name: %s",
				      strerror(errno));
				return MHD_HTTP_INTERNAL_SERVER_ERROR;
			}
			unlink(template);
			free(template);

			res = directory_index(fd, resp->conf, url,
					      resp->file_name,
					      get_sort_col(conn),
					      get_sort_ord(conn));
			if (res) {
				close(fd);
				return errno_to_http_code(res);
			}
			fstat(fd, &resp->st);
			resp->fd = fd;
			resp->type = "text/html";
			return MHD_HTTP_OK;
		} else 
			return res;
	} else if (!S_ISREG(resp->st.st_mode)) {
		return MHD_HTTP_FORBIDDEN;
	}

	resp->fd = open(resp->file_name, O_RDONLY);
	if (resp->fd == -1) {
		error("can't open %s: %s",
		      resp->file_name, strerror(errno));
		return errno_to_http_code(errno);
	}
	fstat(resp->fd, &resp->st);
	resp->type = get_file_type(resp->file_name);

	return MHD_HTTP_OK;
}

static inline char const *
safestr(char const *s)
{
	return s ? s : "-";
}

void 
http_log(struct MHD_Connection *connection,
	 char const *method, char const *url,
	 int status, struct stat const *st)
{
        char *ipstr;
	char const *host, *referer, *user_agent;
	time_t t;
	struct tm *tm;
	char tbuf[30];

	ipstr = get_remote_ip(connection);

	host = MHD_lookup_connection_value(connection,
					   MHD_HEADER_KIND,
					   MHD_HTTP_HEADER_HOST);
	referer = MHD_lookup_connection_value(connection,
					      MHD_HEADER_KIND,
					      MHD_HTTP_HEADER_REFERER);
	user_agent = MHD_lookup_connection_value(connection,
						 MHD_HEADER_KIND,
						 MHD_HTTP_HEADER_USER_AGENT);

	t = time(NULL);
	tm = localtime(&t);
	strftime(tbuf, sizeof(tbuf), "[%d/%b/%Y:%H:%M:%S %z]", tm);
	
	info("%s %s - - %s \"%s %s\" %3d %lu \"%s\" \"%s\"",
	     safestr(host), ipstr, tbuf, method, url, status, 
             st ? st->st_size : 0, safestr(referer), safestr(user_agent));
	free(ipstr);
}

static int
http_redirect(struct MHD_Connection *connection,
	      char const *method, char const *url,
	      int status, char const *loc)
{
	int ret;
	struct MHD_Response *response;
	
	http_log(connection, method, url, status, NULL);
	response = MHD_create_response_from_buffer(0,
						   "",
						   MHD_RESPMEM_PERSISTENT);
	MHD_add_response_header(response,
				MHD_HTTP_HEADER_LOCATION,
				loc);

	ret = MHD_queue_response(connection, status, response);
	MHD_destroy_response(response);
	return ret;
}

static MHD_HANDLER_RETTYPE
fileserv_handler(void *cls,
		 struct MHD_Connection *conn,
		 const char *url, const char *method,
		 const char *version,
		 const char *upload_data, size_t *upload_data_size,
		 void **con_cls)
{
	static int aptr;
	FILE_RESP resp;
	struct MHD_Response *response;
	int ret;
	int status;
	
	if (strcmp(method, MHD_HTTP_METHOD_GET) &&
	    strcmp(method, MHD_HTTP_METHOD_HEAD))
		return http_error(conn, method, url,
				  MHD_HTTP_METHOD_NOT_ALLOWED);

	if (*con_cls == NULL) {
		*con_cls = &aptr;
		return MHD_YES;
	}

	status = get_file_resp(conn, url, &resp);
	switch (status) {
	case MHD_HTTP_OK:
		break;
	case MHD_HTTP_MOVED_PERMANENTLY:
	case MHD_HTTP_FOUND:
		ret = http_redirect(conn, method, url, status,
				    resp.location);
		file_resp_free(&resp);
		return ret;
	default:
		file_resp_free(&resp);
		return http_error(conn, method, url, status);
	}
	
	response = MHD_create_response_from_fd64(resp.st.st_size, resp.fd);
	if (response) {
		resp.fd = -1;
		if (resp.type)
			MHD_add_response_header(response,
						MHD_HTTP_HEADER_CONTENT_TYPE,
						resp.type);
		ret = MHD_queue_response(conn, MHD_HTTP_OK, response);
		MHD_destroy_response(response);
		http_log(conn, method, url, MHD_HTTP_OK, &resp.st);
	} else
		ret = http_error(conn, method, url,
				 MHD_HTTP_INTERNAL_SERVER_ERROR);
	file_resp_free(&resp);
	
	return ret;
}

static void
fileserv_error_printer (char const *msg)
{
	error("%s", msg);
}

static int fatal_signals[] = {
	SIGHUP,
	SIGINT,
	SIGQUIT,
	SIGTERM,
	0
};

static void
http_server(int fd, struct sockaddr *server_addr)
{
	struct MHD_Daemon *mhd;
	sigset_t sigs;
	int i;
		
	/* Block the 'fatal signals' and SIGPIPE in the handling thread */
	sigemptyset(&sigs);
	for (i = 0; fatal_signals[i]; i++)
		sigaddset(&sigs, fatal_signals[i]);
	sigaddset(&sigs, SIGPIPE);
	
	pthread_sigmask(SIG_BLOCK, &sigs, NULL);
	MHD_set_panic_func(fileserv_panic, NULL);

	mhd = MHD_start_daemon(MHD_USE_INTERNAL_POLLING_THREAD
			       | MHD_USE_ERROR_LOG, 0,
			       fileserv_acl, server_addr, 
			       fileserv_handler, NULL,
			       MHD_OPTION_EXTERNAL_LOGGER, fileserv_logger,
			       NULL,
			       MHD_OPTION_LISTEN_SOCKET, fd,
			       MHD_OPTION_END);
	/* Unblock only the fatal signals */
	sigdelset(&sigs, SIGPIPE);
	pthread_sigmask(SIG_UNBLOCK, &sigs, NULL);
	/* Wait for signal to arrive */
	sigwait(&sigs, &i);
	MHD_stop_daemon(mhd);
}	

enum state {
	RUNNING,         /* Program is running */
	TERMINATING,     /* Program is terminating */
	CHILD_TERM,      /* SIGTERM has been sent to the child */
	CHILD_KILL
};
volatile enum state state;

static void
sigterm(int sig)
{
	if (state == RUNNING)
		state = TERMINATING;
}

static void
sigalrm(int sig)
{
	if (state == CHILD_TERM)
		state = CHILD_KILL;
}

static void
http_sentinel(int fd, struct sockaddr *server_addr)
{
	pid_t pid = 0;
	int i;
	struct sigaction act;
	pid_t child_pid = 0;
	int status;

	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);

	act.sa_handler = sigalrm;
	sigaction(SIGALRM, &act, NULL);
	
	for (i = 0; fatal_signals[i]; i++)
		sigaddset(&act.sa_mask, fatal_signals[i]);
	act.sa_handler = sigterm;
	for (i = 0; fatal_signals[i]; i++)
		sigaction(fatal_signals[i], &act, NULL);
		      
	while (1) {
		if (pid == 0) {
			if (state != RUNNING)
				break;
			pid = fork();
			if (pid == -1) {
				error("fork: %s", strerror(errno));
				break;
			}
			if (pid == 0) {
				runas(user, group);
				http_server(fd, server_addr);
				exit(0);
			}
		}
		
		if (child_pid > 0) {
			child_pid = 0;
			if (WIFEXITED(status)) {
				int code = WEXITSTATUS(status);
				if (code || verbose > 1)
					error("child exited with status %d",
					      code);
			} else if (WIFSIGNALED(status)) {
				char const *coremsg = "";
#ifdef WCOREDUMP
				if (WCOREDUMP(status))
					coremsg = " (core dumped)";
#endif
				error("child terminated on signal %d%s",
				      WTERMSIG(status), coremsg);
			} else if (WIFSTOPPED(status)) {
				error("child stopped on signal %d",
				      WSTOPSIG(status));
				continue;
			} else {
				error("child terminated with unrecognized status %d", status);
			}
			/* restart the child */
			pid = 0;
			continue;
		}

		switch (state) {
		case RUNNING:
			break;
			
		case TERMINATING:
			kill(pid, SIGTERM);
			alarm(5);
			state = CHILD_TERM;
			break;

		case CHILD_TERM:
			break;

		case CHILD_KILL:
			kill(pid, SIGKILL);
			return;
		}

		child_pid = wait(&status);
		if (child_pid == -1) {
			if (errno != EINTR || verbose > 1)
				error("wait: %s", strerror(errno));
		}		
	}
}

int
main(int argc, char **argv)
{
	int c, i;
	int fd = -1;
	struct sockaddr *server_addr;
	int foreground = 0;
	int single_process = 0;
	char *opt_address = NULL;
	char *opt_user = NULL;
	char *opt_group = NULL;
	
	progname = basename(argv[0]);

	mimetypes_error_printer = fileserv_error_printer;
	tmpdir = getenv("TMP");
	if (!tmpdir)
		tmpdir = "/tmp";
	
	while ((c = getopt(argc, argv, "a:c:fg:hsu:vV")) != EOF) {
		switch (c) {
		case 'a':
			opt_address = optarg;
			break;
		case 'c':
			config_file = optarg;
			break;
		case 'f':
			foreground = 1;
			break;
		case 'g':
			opt_group = optarg;
			break;
		case 'h':
			usage();
			exit(0);
		case 's':
			single_process = 1;
			break;
		case 'u':
			opt_user = optarg;
			break;
		case 'v':
			verbose++;
			break;
		case 'V':
			show_version();
			exit(0);
		default:
			exit(1);
		}
	}

	readconfig();

	if (opt_address)
		address = xstrdup(opt_address);
	if (opt_user)
		user = xstrdup(opt_user);
	if (opt_group)
		group = xstrdup(opt_group);
	
	pidfile_check();

	if (mime_types_file) {
		mimetypes_parse(mime_types_file); //FIXME: diag
	}
	
	fd = open_listener(address, &server_addr);

	if (single_process)
		runas(user, group);
	
	for (i = optind; i < argc; i++)
		urimap_add(argv[i]);

	if (TAILQ_EMPTY(&map_head)) {
		error("no mappings");
		exit(1);
	}
	
	if (!foreground) {
		if (daemon(0, 1)) {
			error("daemon failed: %s", strerror(errno));
			exit(1);
		}
		for (i = 0; i < fd; i++)
			close(i);
		syslog_enable();
	}
	pidfile_create();

	if (single_process)
		http_server(fd, server_addr);
	else 
		http_sentinel(fd, server_addr);
	
	pidfile_remove();
	return 0;
}


/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */
typedef struct dirlsent {
	char *name;                  /* File name */
	struct stat st;              /* Meta-data */
	char const *type;            /* Mime type */
	STAILQ_ENTRY(dirlsent) next;
} DIRLSENT;

typedef struct dirls {
	size_t count;
	STAILQ_HEAD(, dirlsent) list;
} DIRLS;

void dirls_init(DIRLS *dirls);
void dirls_free(DIRLS *dirls);
int dirls_scan(DIRLS *dirls, char const *path, CONFIG const *conf);
void dirls_sort(DIRLS *dirls, INDEX_SORT_COL col, INDEX_SORT_ORD ord);


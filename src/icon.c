/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */

#include "fileserv.h"
#include <fnmatch.h>
#include <string.h>

typedef TAILQ_HEAD(icon_list,icon) ICON_LIST;

static void
add_icon(ICON_LIST *lst, char const *type, char const *src, char const *alt)
{
	ICON *icon;

	icon = xcalloc(1, sizeof(icon[0]));
	icon->name = xstrdup(type);
	icon->src = xstrdup(src);
	icon->alt = alt ? xstrdup(alt) : NULL;

	TAILQ_INSERT_TAIL(lst, icon, link);
}

static ICON_LIST mime_icon_list = TAILQ_HEAD_INITIALIZER(mime_icon_list);

ICON const *
icon_by_mime(char const *type)
{
	ICON *icon;
	
	TAILQ_FOREACH(icon, &mime_icon_list, link) {
		if (fnmatch(icon->name, type, 0) == 0)
			return icon;
	}
	return NULL;
}

void
add_icon_by_mime(char const *type, char const *src, char const *alt)
{
	return add_icon(&mime_icon_list, type, src, alt);
}

static ICON_LIST suf_icon_list = TAILQ_HEAD_INITIALIZER(suf_icon_list);

void
add_icon_by_name(char const *name, char const *src, char const *alt)
{
	ICON *icon, *cur;
	size_t len, i;
	
	icon = xcalloc(1, sizeof(icon[0]));
	len = strlen(name);
	icon->name = xmalloc(len + 1);
	for (i = 0; i < len; i++)
		icon->name[i] = name[len-i-1];
	icon->name[i] = 0;
	icon->src = xstrdup(src);
	icon->alt = alt ? xstrdup(alt) : NULL;

	TAILQ_FOREACH(cur, &suf_icon_list, link) {
		if (strlen(cur->name) < len) {
			TAILQ_INSERT_BEFORE(cur, icon, link);
			return;
		}
	}
	TAILQ_INSERT_TAIL(&suf_icon_list, icon, link);
}

static int
sufcmp(char const *suf, char const *end, char const *beg)
{
	while (*suf) {
		if (end == beg || *suf++ != *--end)
			return 1;
	}
	return 0;
}

ICON const *
icon_by_name(char const *name)
{
	ICON *icon;
	char const *end = name + strlen(name);
	
	TAILQ_FOREACH(icon, &suf_icon_list, link) {
		if (sufcmp(icon->name, end, name) == 0)
			return icon;
	}
	return NULL;
}

static ICON_LIST type_icon_list = TAILQ_HEAD_INITIALIZER(type_icon_list);

ICON const *
icon_by_type(char const *type)
{
	ICON *icon;
	
	TAILQ_FOREACH(icon, &type_icon_list, link) {
		if (strcmp(icon->name, type) == 0)
			return icon;
	}
	return NULL;
}

void
add_icon_by_type(char const *name, char const *src, char const *alt)
{
	return add_icon(&type_icon_list, name, src, alt);
}


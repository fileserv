/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef _MIMETYPES_LOCUS_H
#define _MIMETYPES_LOCUS_H

#include <string.h>
#include <stdarg.h>

struct locus_point
{
  char const *file;
  unsigned line;
  unsigned col;
};

#define LOCUS_POINT_INITIALIZER { NULL, 0, 0 }

struct locus_range
{
  struct locus_point beg;
  struct locus_point end;
};

#define LOCUS_RANGE_INITIALIZER \
  { LOCUS_POINT_INITIALIZER, LOCUS_POINT_INITIALIZER }

typedef struct linetrack *linetrack_t;

struct linetrack_stat
{
  size_t n_files; /* Number of source files */
  size_t n_lines; /* Number of lines, including the recent (incomplete) one */
  size_t n_chars; /* Total number of characters */
};
  
int ident_ref (char const *name, char const **refname);
int ident_deref (char const *);

int locus_point_set_file (struct locus_point *pt, const char *filename);
void locus_point_init (struct locus_point *pt);
void locus_point_deinit (struct locus_point *pt);
int locus_point_copy (struct locus_point *dest,
			 struct locus_point const *src);

void locus_range_init (struct locus_range *dest);
int locus_range_copy (struct locus_range *dest, struct locus_range const *src);
void locus_range_deinit (struct locus_range *lr);

static inline int
locus_point_same_file (struct locus_point const *a,
		       struct locus_point const *b)
{
  return a->file == b->file
         || (a->file && b->file && strcmp(a->file, b->file) == 0);
}

static inline int
locus_point_same_line (struct locus_point const *a,
			  struct locus_point const *b)
{
  return locus_point_same_file (a, b) && a->line == b->line;
}

static inline int
locus_point_eq (struct locus_point const *a,
		struct locus_point const *b)
{
  return locus_point_same_line (a, b) && a->col == b->col;
}

int linetrack_create (linetrack_t *ret,
			   char const *file_name, size_t max_lines);
int linetrack_origin (linetrack_t trk, struct locus_point const *pt);
int linetrack_rebase (linetrack_t trk, struct locus_point const *pt);
void linetrack_free (linetrack_t trk);
void linetrack_destroy (linetrack_t *trk);
void linetrack_advance (linetrack_t trk,
			struct locus_range *loc,
			char const *text, size_t leng);
int linetrack_retreat (linetrack_t trk, size_t n);

int linetrack_locus (struct linetrack *trk, struct locus_point *lp);
int linetrack_stat (linetrack_t trk, struct linetrack_stat *st);
int linetrack_at_bol (struct linetrack *trk);

void lrange_debug (struct locus_range const *loc, char const *fmt, ...);
ssize_t locus_range_format (char **pbuf, size_t *psize, struct locus_range const *lr);


#endif

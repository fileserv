/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __YYLOC_H
#define __YYLOC_H

#define YYLTYPE struct locus_range
#define YYLLOC_DEFAULT(Current, Rhs, N)				  \
  do								  \
    {								  \
      if (N)							  \
	{							  \
	  (Current).beg = YYRHSLOC(Rhs, 1).beg;			  \
	  (Current).end = YYRHSLOC(Rhs, N).end;			  \
	}							  \
      else							  \
	{							  \
	  (Current).beg = YYRHSLOC(Rhs, 0).end;			  \
	  (Current).end = (Current).beg;			  \
	}							  \
    } while (0)

#define YY_LOCATION_PRINT(File, Loc)     		        \
	print_locus_range (File, &(Loc))

#endif    
  

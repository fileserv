extern void (*mimetypes_error_printer) (char const *);
int mimetypes_parse (const char *name);
const char *get_file_type (char const *filename);
